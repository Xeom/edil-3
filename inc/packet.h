#if !defined(PACKET_H)
# define PACKET_H
# include <stddef.h>
# include <pthread.h>
# include <sys/time.h>

# include "module/input/keys.h"
# include "container/vec.h"

typedef struct packet_s packet_s;
typedef struct packet_interface_s packet_interface_s;

typedef enum
{
    PACKET_SET_LINE,
    PACKET_GET_LINE,
    PACKET_INS_LINE,
    PACKET_DEL_LINE,

    PACKET_BUF_OPEN,
    PACKET_BUF_CLOSE,
    PACKET_BUF_INFO,
    PACKET_BUF_SUBSCRIBE,
    PACKET_BUF_UNSUBSCRIBE,

    PACKET_ADD_DEST,
    PACKET_DEL_DEST,
    PACKET_KILL,

    PACKET_KEY_PRESS,
    PACKET_MOUSE_PRESS,
    PACKET_TYPED,

    PACKET_WIN_SPLIT,
    PACKET_WIN_CLOSE,
    PACKET_WIN_TEXT,
    PACKET_WIN_INFO,
    PACKET_WIN_IFACE,
    PACKET_WIN_CLEAR,
    PACKET_WIN_CLEAR_AT,
    PACKET_WIN_SEL
} packet_type_t;

struct packet_s
{
    size_t bytes;
    size_t width;

    pthread_t     tid;
    packet_type_t type;

    union
    {
        struct { packet_interface_s *iface; } bufopen;
        struct { long buf; packet_interface_s *iface; } bufsubscribe;
        struct { long buf; packet_interface_s *iface; } bufunsubscribe;
        struct { long buf; } bufclose;
        struct { long buf; } bufinfo;
        struct { int win, cols, rows; } wininfo;
        struct { int win;          } winclose;
        struct { int win;          } winclear;
        struct { int win;          } winsel;
        struct { int win, x, y;    } wintext;
        struct { int win, x, y;    } winclearat;
        struct { long buf, ln;     } setline;
        struct { long buf, ln, n, len; } insline;
        struct { long buf, ln, n, len; } delline;
        struct { input_key_t key;  } keypress;
        struct { int x, y, button; } mousepress;

        struct { int win; packet_interface_s *iface;      } winiface;
        struct { int win, dir; packet_interface_s *iface; } winsplit;
        struct { long buf, ln; packet_interface_s *iface; } getline;
    } data;
};

struct packet_interface_s
{
    bool (*fptr_init)(packet_interface_s *, void **);
    bool (*fptr_recv)(packet_interface_s *, void *, packet_s *, vec_s *);
    bool (*fptr_tout)(packet_interface_s *, void *);
    void (*fptr_quit)(packet_interface_s *, void *);

    pthread_mutex_t mtx;
    pthread_t thread;

    struct timeval tout;

    bool tofree;

    int sendfd;
    int recvfd;
};

void packet_interface_init(packet_interface_s *iface);
void packet_interface_kill(packet_interface_s *iface);

void packet_send(packet_interface_s *iface, packet_s *packet, vec_s *data);
int  packet_recv(packet_interface_s *iface, packet_s *packet, vec_s *data);
bool packet_waiting(packet_interface_s *iface, struct timeval tout);

packet_interface_s *packet_interface_create(
    void (*startf)(packet_interface_s *));
#endif
