#if !defined(CONTAINER_VEC_H)
# define CONTAINER_VEC_H
# include <stdbool.h>
# include <stddef.h>
# include <string.h>

typedef struct vec_s vec_s;

struct vec_s
{
    char   *data;
    size_t  capacity;
    size_t  usage;
    size_t  width;
    bool    resizable;
};

void vec_init(vec_s *v, size_t width);

void vec_init_from_mem(vec_s *v, size_t width, void *mem, size_t num);

void vec_kill(vec_s *v);

int vec_resize(vec_s *v, size_t num);

int vec_sort(vec_s *v, int (*f)(const void *a, const void *b));

void *vec_rep(vec_s *v, size_t ind, size_t n, const void *data, size_t reps);

int vec_del(vec_s *v, size_t ind, size_t n);

void *vec_get(vec_s *v, size_t ind);
static inline size_t vec_len(vec_s *v)
{
    if (!v || !(v->width)) return 0;

    return v->usage / v->width;
}

static inline void *vec_first(vec_s *v)
{
    if (!v) return NULL;

    return v->data;
}

static inline void *vec_last(vec_s *v)
{
    return vec_get(v, vec_len(v) - 1);
}

static inline void *vec_ins(vec_s *v, size_t ind, size_t n, void *data)
{
    return vec_rep(v, ind, n, data, 1);
}

static inline void *vec_add(vec_s *v, size_t n, void *data)
{
    return vec_rep(v, vec_len(v), n, data, 1);
}

static inline void *vec_app(vec_s *v, void *data)
{
    return vec_rep(v, vec_len(v), 1, data, 1);
}

static inline void *vec_cpy(vec_s *v, vec_s *other)
{
    if (!vec_len(other)) return NULL;

    return vec_rep(v, vec_len(v), vec_len(other), vec_first(other), 1);
}

static inline void vec_clr(vec_s *v)
{
    vec_del(v, 0, vec_len(v));
}

static inline int vec_pop(vec_s *v, size_t n)
{
    return vec_del(v, vec_len(v) - n, n);
}

static inline void vec_str(vec_s *v, char *str)
{
    vec_rep(v, vec_len(v), strlen(str), str, 1);
}


#define VEC_RFOREACH(_vec, _type, _var) \
    _type _var;                         \
    for (long _ind = vec_len(_vec) - 1; \
         _var = vec_get(_vec, _ind),    \
         _ind >= 0    ;                 \
         --_ind)

#define VEC_FOREACH(_vec, _type, _var) \
    _type _var;                        \
    for (size_t _ind = 0,              \
         _len = vec_len(_vec);         \
         _var = vec_get(_vec, _ind),   \
         _ind < _len;                  \
         ++_ind)

#endif
