#if !defined(TABLE_H)
# define TABLE_H
# include <stddef.h>

typedef struct table_s table_s;

struct table_s
{
    size_t capacity; /* The number of blocks memory is allocated for */
    size_t usage;    /* The number of blocks used                    */
    size_t valwidth; /* The sizeof the values being stored           */
    size_t keywidth; /* The sizeof the keys being store              */
    size_t blkwidth; /* The total size of a block                    */
    char  *data;     /* The array of blocks                          */
};

/* Initialize a table */
void table_init(table_s *t, size_t valwidth, size_t keywidth);

/* Kill a table */
void table_kill(table_s *t);

/* Get the number of elements in a table */
size_t table_len(table_s *t);

/* Associate a value with a key in the table */
void *table_set(table_s *t, const void *k, const void *value);

/* Find the value associated with a key in the table */
void *table_get(table_s *t, const void *k);

/* Remove a key from the table */
void table_delete(table_s *t, const void *k);

void *table_next(table_s *t, void *val, void **key);
#endif
