#if !defined(MODULE_WELCOME_CORE_H)
# define MODULE_WELCOME_CORE_H
# include "packet.h"

typedef struct welcome_ctx_s welcome_ctx_s;

struct welcome_ctx_s
{
    int winid;
    int rows, cols;
};

void welcome_start(packet_interface_s *iface);

#endif
