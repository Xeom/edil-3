#if !defined(MODULE_VIEWER_H)
# define MODULE_VIEWER_H
# include "module/buffers/core.h"
# include "packet.h"

typedef struct editor_ctx_s editor_ctx_s;

struct editor_ctx_s
{
    int win;
    long buf;
    int scrx, scry;
    int curx, cury;
    int cols, rows;
};

void editor_start(packet_interface_s *iface);

#endif
