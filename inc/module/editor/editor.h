#if !defined(MODULE_EDITOR_H)
# define MODULE_EDITOR_H
# include "module/editor/core.h"
# include "text/chr.h"

chr_s *editor_get_line_marker(editor_ctx_s *ctx, long ln);

void editor_request_after(editor_ctx_s *ctx, packet_interface_s *iface, long ln);

void editor_request(editor_ctx_s *ctx, packet_interface_s *iface, long ln);

chr_s *editor_get_line_marker(editor_ctx_s *ctx, long ln);

void editor_handle_update(editor_ctx_s *ctx, long buf, long ln, vec_s *chrs);

#endif
