#if !defined(MODULE_INPUT_CORE_H)
# define MODULE_INPUT_CORE_H
# include "module/input/parser.h"

typedef struct input_ctx_s input_ctx_s;

struct input_ctx_s
{
    input_parser_ctx_s parser;
    int  fd;
    bool alive;
    pthread_t thread;
};

void input_start(packet_interface_s *iface);

#endif
