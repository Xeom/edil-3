#if !defined(MODULE_INPUT_KEYS_H)
# define MODULE_INPUT_KEYS_H

typedef enum
{
    KEY_CTRL  = 0x400,
    KEY_ESC   = 0x200,
    KEY_NONE  = 0x100,
    KEY_UP,
    KEY_DOWN,
    KEY_LEFT,
    KEY_RIGHT,
    KEY_HOME,
    KEY_END,
    KEY_PGDN,
    KEY_PGUP,
    KEY_INS,
    KEY_TAB,
    KEY_SHIFTTAB,
    KEY_DEL,
    KEY_SHIFTDEL,
    KEY_BACK,
    KEY_ENTER,
    KEY_PASTEBEGIN,
    KEY_PASTEEND,
    KEY_F1,
    KEY_F2,
    KEY_F3,
    KEY_F4,
    KEY_F5,
    KEY_F6,
    KEY_F7,
    KEY_F8,
    KEY_F9,
    KEY_F10,
    KEY_F11,
    KEY_F12,
    KEY_MOUSECLICK
} input_key_t;

# include "module/input/parser.h"

void input_keys_add(input_parser_ctx_s *ctx);

#endif
