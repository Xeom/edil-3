#if !defined(MODULE_INPUT_PARSER_H)
# define MODULE_INPUT_PARSER_H
# include "container/vec.h"
# include "text/chr.h"
# include <stdbool.h>

typedef struct input_kmap_s input_kmap_s;
typedef struct input_parser_ctx_s input_parser_ctx_s;

# include "module/input/keys.h"

struct input_kmap_s
{
    input_key_t   keys[256];
    input_kmap_s *subs[256];
};

struct input_parser_ctx_s
{
    bool escaped;
    bool mouseclick;

    char seq[16];
    int  seqind;

    vec_s chrs;
    chr_s chr;

    input_kmap_s  root;
    input_kmap_s *current;
};

void input_parser_flush_str(input_parser_ctx_s *ctx);

void input_parse(input_parser_ctx_s *ctx, unsigned char c);

void input_parser_ctx_init(input_parser_ctx_s *ctx);
void input_parser_ctx_kill(input_parser_ctx_s *ctx);

void input_parser_add_key(input_parser_ctx_s *ctx, char *combo, input_key_t key);

#endif
