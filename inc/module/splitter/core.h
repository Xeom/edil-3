#if !defined(MODULE_SPLITTER_CORE__H)
# define MODULE_SPLITTER_CORE_H

void splitter_start(packet_interface_s *iface);

void splitter_add_dest(packet_interface_s *iface, packet_interface_s *dest);

void splitter_del_dest(packet_interface_s *iface, packet_interface_s *dest);

#endif
