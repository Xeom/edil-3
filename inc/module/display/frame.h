#if !defined(MODULE_DISPLAY_H)
# define MODULE_DISPLAY_H
# include "container/vec.h"
# include "packet.h"

typedef struct frame_s       frame_s;
typedef struct frame_dims_s  frame_dims_s;
typedef enum
{
    WIN_SPLIT_HORIZONTAL = 'h',
    WIN_SPLIT_VERTICAL   = 'v',
    WIN_SPLIT_NONE       = 'x'
} frame_split_t;

# include "module/display/display.h"

struct frame_dims_s
{
    int x, y;
    int w, h;
};

struct frame_s
{
    struct frame_s *parent;
    struct frame_s *child1;
    struct frame_s *child2;

    frame_split_t   dir;
    float           adj;
    frame_dims_s    dims;

    packet_interface_s *win;
    int curx, cury;
};

void frame_update_dims(frame_s *frm, display_ctx_s *d);

frame_s *frame_split(frame_s *frm, display_ctx_s *d, frame_split_t dir);

void frame_free(frame_s *frm);

int frame_close(frame_s *frm, display_ctx_s *d);

void frame_print(frame_s *frm, display_ctx_s *d);

void frame_print_content(frame_s *frm, int x, int y, vec_s *chrs);

int frame_send_info(frame_s *frm, int id);

void frame_clear(frame_s *frm, display_ctx_s *d);

void frame_clear_at(frame_s *frm, display_ctx_s *d, int x, int y);

void frame_reset_cursor(frame_s *frm);

#endif
