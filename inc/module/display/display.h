#if !defined(MODULE_DISPLAY_DISPLAY_H)
# define MODULE_DISPLAY_DISPLAY_H
# include "container/vec.h"
# include "packet.h"

typedef struct display_ctx_s display_ctx_s;

# include "module/display/frame.h"

struct display_ctx_s
{
    int      rows, cols;
    frame_s *root;
    vec_s    frames;
    bool      winch;
    pthread_t winch_thread;
    int       sel;
};

frame_s *display_get_frame(display_ctx_s *d, int id);

void display_print(display_ctx_s *d);

frame_s *display_get_selected(display_ctx_s *d);

int display_add_frame(display_ctx_s *d, frame_s *frm);

void display_del_frame(display_ctx_s *d, int id);

int display_split(
    display_ctx_s *d, int id, frame_split_t dir, packet_interface_s *iface);

int display_close(display_ctx_s *d, int id);

int display_set_iface(display_ctx_s *d, int id, packet_interface_s *iface);

int display_send_info(display_ctx_s *d, int id);

int display_clear(display_ctx_s *d, int id);

int display_clear_at(display_ctx_s *d, int id, int x, int y);

int display_print_content(display_ctx_s *d, int id, int x, int y, vec_s *chrs);

void display_send_info_all(display_ctx_s *d);

void display_kill(display_ctx_s *d);

#endif
