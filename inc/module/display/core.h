#if !defined(MODULE_DISPLAY_CORE_H)
# define MODULE_DISPLAY_CORE_H
# include "module/display/frame.h"
# include "packet.h"

void display_start(packet_interface_s *iface);

void display_send_chrs(packet_interface_s *iface, int x, int y, int win, vec_s *text);

void display_send_format(packet_interface_s *iface, int x, int y, int win, char *fmt, ...);

void display_send_str(packet_interface_s *iface, int x, int y, int win, char *str);

void display_send_clear(packet_interface_s *iface, int win);

void display_send_clear_at(packet_interface_s *iface, int x, int y, int win);

void display_send_split(
    packet_interface_s *iface, int win, frame_split_t dir, packet_interface_s *winiface);

void display_send_close(packet_interface_s *iface, int win);

void display_send_win_iface(
    packet_interface_s *iface, int win, packet_interface_s *winiface);

#endif
