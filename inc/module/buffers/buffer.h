#if !defined(MODULE_BUFFERS_BUFFER_H)
# define MODULE_BUFFERS_BUFFER_H
# include "container/vec.h"
# include "packet.h"

typedef struct buffer_s buffer_s;
struct buffer_s
{
    vec_s subscribed;
    vec_s lines;
};

void buffer_init(buffer_s *buf);
void buffer_kill(buffer_s *buf);

int buffer_get_line(buffer_s *buf, long ln, vec_s *chrs);
int buffer_set_line(buffer_s *buf, long ln, vec_s *chrs);

int buffer_ins_lines(buffer_s *buf, long ln, long n);
int buffer_del_lines(buffer_s *buf, long ln, long n);

int buffer_subscribe(buffer_s *buf, packet_interface_s *iface);
int buffer_unsubscribe(buffer_s *buf, packet_interface_s *iface);
int buffer_send(buffer_s *buf, packet_s *pkt, vec_s *data);

size_t buffer_len(buffer_s *buf);

#endif
