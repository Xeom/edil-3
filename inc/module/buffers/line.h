#if !defined(MODULE_BUFFERS_LINE_H)
# define MODULE_BUFFERS_LINE_H
# include "container/vec.h"

typedef struct line_s line_s;

struct line_s
{
    vec_s chrs;
};

void line_init(line_s *l);
void line_kill(line_s *l);

void line_set(line_s *l, vec_s *chrs);
void line_get(line_s *l, vec_s *chrs);

#endif
