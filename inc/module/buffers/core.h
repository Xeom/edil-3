#if !defined(MODULE_BUFFERS_CORE_H)
# define MODULE_BUFFERS_CORE_H
# include "container/vec.h"
# include "module/buffers/buffer.h"
# include "packet.h"

typedef struct buffers_ctx_s buffers_ctx_s;
struct buffers_ctx_s
{
    vec_s buffers;
};

void buffers_start(packet_interface_s *iface);

void buffers_send_get_line(long bn, long ln, packet_interface_s *rtn);
void buffers_send_set_line(long bn, long ln, vec_s *text);

void buffers_send_ins_line(long bn, long ln, long n);
void buffers_send_del_line(long bn, long ln, long n);

void buffers_send_open(packet_interface_s *sub);
void buffers_send_close(long bn);

void buffers_send_unsubscribe(long bn, packet_interface_s *iface);
void buffers_send_subscribe(long bn, packet_interface_s *iface);

#endif
