#if !defined(CORE_H)
# define CORE_H
# include "packet.h"

extern packet_interface_s display;
extern packet_interface_s line_splitter;
extern packet_interface_s buffers;

void core_init(void);

#endif
