#if !defined(UTIL_LOG_H)
# define UTIL_LOG_H
# include <stdio.h>

#if !defined(NO_LOG)
# if !defined(NO_LOG_KEYS)
#  define LOG_KEYS(...) log_print("KEYS",   __VA_ARGS__)
# else
#  define LOG_KEYS(...) do { } while (0)
# endif

# if !defined(NO_LOG_PKTS)
#  define LOG_PKTS(...) log_print("PACKET", __VA_ARGS__)
# else
#  define LOG_KEYS(...) do { } while (0)
# endif

# if !defined(NO_LOG_TEXT)
#  define LOG_TEXT(...) log_print("TEXT",   __VA_ARGS__)
# else
#  define LOG_KEYS(...) do { } while (0)
# endif

# if !defined(NO_LOG_INFO)
#  define LOG_INFO(...) log_print("INFO",   __VA_ARGS__)
# else
#  define LOG_KEYS(...) do { } while (0)
# endif

# if !defined(NO_LOG_WARN)
#  define LOG_WARN(...) log_print("WARN",   __VA_ARGS__)
# else
#  define LOG_KEYS(...) do { } while (0)
# endif

# if !defined(NO_LOG_ERR)
#  define LOG_ERR(...)  log_print("ERROR",  __VA_ARGS__)
# else
#  define LOG_KEYS(...) do { } while (0)
# endif
#endif

#define LOG(type, ...) \
    LOG_ ## type(__VA_ARGS__)

extern FILE *log_file;

void log_print(const char *type, const char *fmt, ...);

#endif
