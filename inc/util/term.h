#if !defined(UTIL_TERM_H)
# define UTIL_TERM_H

#define TERM_GOTO(cn, ln) "\033[" #ln ";" #cn "H"
#define TERM_CLR_LINE     "\033[K"
#define TERM_CLR_SCREEN   "\033[2J"
#define TERM_HIDE_CUR     "\033[?25l"
#define TERM_SHOW_CUR     "\033[?25h"
#define TERM_RESET_COL    "\033[0m"

#define TERM_UP    "\033[A"
#define TERM_DOWN  "\033[B"
#define TERM_RIGHT "\033[C"
#define TERM_LEFT  "\033[D"

static inline void term_goto(int x, int y)
{
    printf(TERM_GOTO(%d, %d), y, x);
}

void term_on_winch(void (*fptr)(int));

void term_get_dims(int *cols, int *rows);

void term_init(void);
void term_kill(void);

#endif
