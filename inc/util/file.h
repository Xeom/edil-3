#if !defined(UTIL_FILE_H)
# define UTIL_FILE_H

typedef struct file_s file_s;

typedef enum
{
    file_cr    = 0x01,
    file_pipe  = 0x02,
    file_eofnl = 0x04
} file_flags;

struct file_s
{
    vec_s fname;
    vec_s dirname;
    vec_s basename;
    FILE *fptr;
    file_flags flags;
};

void file_init(file_s *f);

void file_init_pipe(file_s *f, FILE *pipe);

void file_kill(file_s *f);

char *file_base(file_s *f);

char *file_name(file_s *f);

int file_assoc(file_s *f, vec_s *chrname);

void file_deassoc(file_s *f);

int file_associated(file_s *f);

int file_exists(file_s *f);

int file_ended(file_s *f);

int file_open(file_s *f, const char *mode);

int file_close(file_s *f);

void file_kill(file_s *f);

int file_read_line(file_s *f, vec_s *line);

int file_set_paths(file_s *f, vec_s *chrname);

#endif
