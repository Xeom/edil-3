#if !defined(UTIL_SYS_H)
# define UTIL_SYS_H

int sys_tid(void);

void sys_sig_wait(int sign);

void sys_sig_block(int sign);

#endif
