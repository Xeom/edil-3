#if !defined(TEXT_COL_H)
# define TEXT_COL_H
# include <stdint.h>


typedef struct col_rgb_s  col_rgb_s;
typedef struct col_desc_s col_desc_s;

struct col_rgb_s
{
    uint8_t r, g, b;
};

typedef enum
{
    COL_UNDER = 0x01,
    COL_REV   = 0x02,
    COL_FLASH = 0x04,
    COL_NOFG  = 0x08,
    COL_NOBG  = 0x10,
    COL_NONE  = 0x18
} col_attrs_t;

struct col_desc_s
{
    col_rgb_s fg, bg;
    col_attrs_t attrs;
};

typedef enum
{
    COL_DEFAULT,
    COL_WIN_FRAME,
    COL_WIN_FRAME_SEL
} col_t;

void col_print(col_t col);

#endif
