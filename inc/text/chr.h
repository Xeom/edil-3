#if !defined(TEXT_CHR_H)
# define TEXT_CHR_H
# include <stdio.h>
# include <stdint.h>
# include <stdbool.h>

# include "container/vec.h"

#define CHR(s) ((chr_s){ .utf8 = s })

typedef struct chr_s chr_s;

typedef enum
{
    CHR_UTF8_ASCII = 1,
    CHR_UTF8_2WIDE = 2,
    CHR_UTF8_3WIDE = 3,
    CHR_UTF8_4WIDE = 4,
    CHR_UTF8_5WIDE = 5,
    CHR_UTF8_6WIDE = 6,
    CHR_UTF8_PART,
    CHR_UTF8_INV
} chr_utf8_t;

struct chr_s
{
    char utf8[7];
    bool blank;
};


chr_utf8_t chr_get_utf8_type(unsigned char c);


int chr_utf8_type_len(chr_utf8_t type);
int chr_utf8_len(chr_s *chr);

int chr_width(chr_s *chr);

void chr_print(chr_s *chr, FILE *stream, bool colour);

uint32_t chr_codepoint(chr_s *chr);

int chr_is_whitespace(chr_s *chr);

int chr_decode(chr_s *chr, char c);

void chr_from_mem(vec_s *chrs, char *str, size_t len);
void chr_from_str(vec_s *chrs, char *str);
void chr_from_vec(vec_s *chrs, vec_s *str);

void chr_format(vec_s *chrs, char *fmt, ...);
void chr_va_format(vec_s *chrs, char *fmt, va_list args);

void chr_to_str(vec_s *chrs, vec_s *str);

#endif
