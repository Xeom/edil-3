#if !defined _XOPEN_SOURCE /* For realpath() */
# define _XOPEN_SOURCE 500
# include <stdlib.h>
# include <limits.h>
# undef _XOPEN_SOURCE
#else
# include <stdlib.h>
# include <limits.h>
#endif

#include <errno.h>
#include <libgen.h>
#include <string.h>
#include <unistd.h>

#include "container/vec.h"
#include "text/chr.h"

#include "util/file.h"

void file_init(file_s *f)
{
    vec_init(&(f->fname),    sizeof(char));
    vec_init(&(f->basename), sizeof(char));
    vec_init(&(f->dirname) , sizeof(char));

    f->flags = 0;
    f->fptr = NULL;
}

void file_init_pipe(file_s *f, FILE *pipe)
{
    file_init(f);

    f->flags |= file_pipe;
    f->fptr = pipe;
}

void file_kill(file_s *f)
{
    file_close(f);

    vec_kill(&(f->fname));
    vec_kill(&(f->basename));
    vec_kill(&(f->dirname));
}

char *file_name(file_s *f)
{
    static char *empty = "";

    if (!file_associated(f) || f->flags & file_pipe)
        return empty;

    return vec_first(&(f->fname));
}

char *file_base(file_s *f)
{
    static char *empty = "";

    if (!file_associated(f) || f->flags & file_pipe)
        return empty;

    return vec_first(&(f->basename));
}

int file_assoc(file_s *f, vec_s *chrname)
{
    if (f->flags & file_pipe)
        return -1;

    if (file_associated(f))
        file_deassoc(f);

    return file_set_paths(f, chrname);
}

void file_deassoc(file_s *f)
{
    vec_clr(&(f->fname));
    vec_clr(&(f->basename));
    vec_clr(&(f->dirname));
}

int file_associated(file_s *f)
{
    if (f->flags & file_pipe)
        return 0;

    return vec_len(&(f->fname)) > 0;
}

int file_exists(file_s *f)
{
    return access(vec_first(&(f->fname)), F_OK) != -1;
}

int file_open(file_s *f, const char *mode)
{
    char *path;
    path = vec_first(&(f->fname));

    if (f->flags & file_pipe)
        return 0;

    if (!file_associated(f))
        return -1;

    file_close(f);

    f->fptr = fopen(path, mode);

    if (!(f->fptr))
        return -1;
    else
        return 0;
}

int file_close(file_s *f)
{
    int rtn;

    if (!(f->fptr))
        return 0;

    rtn = fclose(f->fptr);
    f->fptr = NULL;

    return rtn;
}

int file_read_line(file_s *f, vec_s *chrs)
{
    int c;
    chr_s chr;

    memset(&chr, 0, sizeof(chr_s));

    vec_clr(chrs);

    while (!feof(f->fptr))
    {
        c = fgetc(f->fptr);

        if (c == EOF) break;

        if (c == '\r')
        {
            f->flags |= file_cr;
            continue;
        }
        if (c == '\n') break;

        if (chr_decode(&chr, (char)c) == 1)
        {
            vec_app(chrs, &chr);
            memset(&chr, 0, sizeof(chr_s));
        }
    }

    if (ferror(f->fptr))
        return -1;
    else
        return  0;
}

int file_ended(file_s *f)
{
    if (!f->fptr)
        return 1;
    else
        return feof(f->fptr);
}

/* chrs is a vec of chrs, fullpath is of chars */
int file_set_paths(file_s *f, vec_s *chrname)
{
    /* Three copies of the stringified argument.     *
     * The first one to keep, and the second to feed *
     * to basename and dirname.                      */
    vec_s dirvec, basevec;
    char *base, *dir, *path;

    /* Make throwaway copies of our argument path */
    vec_init(&basevec,  sizeof(char));
    vec_init(&dirvec,   sizeof(char));

    chr_to_str(chrname, &basevec);
    vec_app(&basevec, "\0");
    vec_cpy(&dirvec,  &basevec);

    /* We get our base and dir. These should NOT be free'd, as *
     * they may be inside dirvec and basevec. If not, they are *
     * statically allocated memory.                            */
    dir  =  dirname(vec_first(&dirvec));
    base = basename(vec_first(&basevec));

    vec_clr(&(f->basename));
    vec_clr(&(f->dirname));
    vec_clr(&(f->fname));

    if (dir && base)
    {
        /* We must free this */
        vec_str(&(f->basename), base);
        vec_str(&(f->dirname),  dir);

        vec_app(&(f->basename), "\0");
        vec_app(&(f->dirname),  "\0");

        path = realpath(dir, NULL);
    }
    else
        path = NULL;

    if (path)
    {
        vec_str(&(f->fname), path);

        /* Add a trailing slash to fullpath if needed */
        if (path[strlen(path) - 1] != '/')
            vec_app(&(f->fname), "/");

        /* Add the basename to the directory name. Don't do this *
         * if it is ., or /, as these are pretend names.         */
        if (strcmp(base, "/") != 0 && strcmp(base, ".") != 0)
            vec_str(&(f->fname), base);

        /* Null terminator */
        vec_app(&(f->fname), "\0");

        free(path);
    }

    vec_kill(&basevec);
    vec_kill(&dirvec);

    if (path)
        return 0;
    else
    {
        file_deassoc(f);
        return -1;
    }
}
