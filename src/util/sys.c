#include <sys/types.h>
#include <stdbool.h>
#include <pthread.h>
#include <syscall.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include "util/log.h"
#include "util/sys.h"

int sys_tid(void)
{
#if defined(LINUX_TID)
    pid_t tid;
    tid = syscall(__NR_gettid);
#else
    static bool            created = false;
    static pthread_key_t   key;
    static pthread_mutex_t mtx     = PTHREAD_MUTEX_INITIALIZER;
    static int             maxtid  = 0;
    int tid, *ptr;

    pthread_mutex_lock(&mtx);

    if (!created) pthread_key_create(&key, free);
    created = true;

    if (pthread_getspecific(key) == NULL)
    {
        tid  = maxtid++;
        ptr  = malloc(sizeof(int));
        *ptr = tid;
        pthread_setspecific(key, ptr);
    }

    ptr = pthread_getspecific(key);
    tid = *ptr;

    pthread_mutex_unlock(&mtx);
#endif

    return (int)tid;
}

void sys_sig_wait(int sig)
{
    int sign;
    sigset_t set;

    LOG(INFO, "Waiting for signal %d", sig);

    sigemptyset(&set);
    sigaddset(&set, sig);
    sigwait(&set, &sign);
}

void sys_sig_block(int sig)
{
    sigset_t set;

    sigemptyset(&set);
    sigaddset(&set, SIGWINCH);
    pthread_sigmask(SIG_BLOCK, &set, NULL);

    LOG(INFO, "Blocking signal %d", sig);
}
