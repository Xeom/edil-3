#if !defined _POSIX_C_SOURCE
# define _POSIX_C_SOURCE 1
# include <signal.h>
# undef  _POSIX_C_SOURCE
#else
# include <signal.h>
#endif

#include <sys/ioctl.h>
#include <sys/types.h>
#include <termios.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>

#include "util/term.h"

static struct termios term_tattr_orig;

void term_on_winch(void (*fptr)(int))
{
    struct sigaction act;

    act.sa_handler = fptr;
    act.sa_flags = 0;
    sigemptyset(&act.sa_mask);

    sigaction(SIGWINCH, &act, NULL);
}

void term_get_dims(int *cols, int *rows)
{
    struct winsize wsz;

    ioctl(fileno(stdin), TIOCGWINSZ, &wsz);

    *cols = wsz.ws_col;
    *rows = wsz.ws_row;
}

void term_init(void)
{
    struct termios   tattr;

    /* Set terminal attributes */
    tcgetattr(STDOUT_FILENO, &tattr);

    memcpy(&term_tattr_orig, &tattr, sizeof(struct termios));

    tattr.c_lflag &= ~(ICANON | ECHO | ISIG);
    tattr.c_iflag &= ~(IXON | IXOFF);
    tattr.c_iflag |= IXANY;

    tattr.c_cc[VMIN]  = 1;
    tattr.c_cc[VTIME] = 0;

    tcsetattr(STDOUT_FILENO, TCSANOW, &tattr);

    /* Clear sreen and hide cursor */
    fputs(TERM_GOTO(0, 0) TERM_CLR_SCREEN, stdout);
}

void term_kill(void)
{
    tcsetattr(STDOUT_FILENO, TCSANOW, &term_tattr_orig);

    fputs(TERM_GOTO(0, 0) TERM_CLR_SCREEN TERM_RESET_COL TERM_SHOW_CUR, stdout);
}
