#include <stdarg.h>

#include "util/sys.h"

#include "util/log.h"

FILE *log_file = NULL;

void log_print(const char *type, const char *fmt, ...)
{
    char    msg[128];
    char    src[32];
    va_list args;

    if (log_file == NULL)
        log_file = fopen("/tmp/edil-log", "w");

    va_start(args, fmt);

    vsnprintf(msg, sizeof(msg), fmt, args);
    snprintf (src, sizeof(src), "(tid %03d) [%s]", sys_tid(), type);

    fprintf(log_file, " %-20s%s\n", src, msg);
    fflush(log_file);
    va_end(args);
}
