#include <stdlib.h>

#include "core.h"
#include "text/chr.h"
#include "util/log.h"
#include "container/vec.h"
#include "module/display/core.h"

#include "module/welcome/core.h"

static char *welcome_banner[] = {
    "--------------------- .----. ",
    ".---..--. .----..-.   '---. \\",
    "| --:| . |'-..-'| |    .--' /",
    "| --:| ' |..''..| '-.  '--. \\",
    "'---''--' '----''---' .---' /",
    "--------------------- '----'",
    " My Editor,",
    "  By Francis Wharf.",
    "",
    "Press Esc+n to open a new file,",
    " or press Ctrl+Esc+k to exit.",
};

static void welcome_update(welcome_ctx_s *ctx)
{
    int x, y, ind;
    const int nlines = sizeof(welcome_banner)/sizeof(char *);

    if (ctx->winid == -1) return;

    x = (ctx->cols - strlen(welcome_banner[0])) / 2;
    y = (ctx->rows - nlines)                    / 3;

    for (ind = 0; ind < nlines; ++ind)
        display_send_str(&display, x, y + ind, ctx->winid, welcome_banner[ind]);

}

static bool welcome_init(packet_interface_s *iface, void **user)
{
    welcome_ctx_s *ctx;

    LOG(INFO, "Welcome started on iface %p", iface);
    ctx = malloc(sizeof(welcome_ctx_s));
    ctx->winid = -1;
    *user = ctx;

    return true;
}

static void welcome_quit(packet_interface_s *iface, void *user)
{
    LOG(INFO, "Welcome exited");
    free(user);
}

static bool welcome_recv(packet_interface_s *iface, void *user, packet_s *pkt, vec_s *data)
{
    welcome_ctx_s *ctx;
    ctx = user;

    switch (pkt->type)
    {
    case PACKET_WIN_INFO:
        {
            ctx->winid = pkt->data.wininfo.win;
            ctx->cols  = pkt->data.wininfo.cols;
            ctx->rows  = pkt->data.wininfo.rows;

            LOG(INFO, "Welcome window updated - #%d, %dx%d",
                ctx->winid, ctx->cols, ctx->rows);

            welcome_update(ctx);
        }
        break;

    case PACKET_WIN_CLOSE:
        {
            int win;

            win = pkt->data.winclose.win;

            LOG(INFO, "Welcome window on iface %p recieved window %d close", iface, win);
            if (win == ctx->winid)
                return false;
        }
        break;

    case PACKET_KEY_PRESS:
        {
            LOG(INFO, "Keypress!!!");
            display_send_format(&display, 0, 0, ctx->winid, "KEYPRESS %d", pkt->data.keypress.key);
        }
        break;
    case PACKET_TYPED:
        {
            display_send_chrs(&display, 0, 0, ctx->winid, data);
        }
        break;
    }

    return true;
}

void welcome_start(packet_interface_s *iface)
{
    iface->fptr_init = welcome_init;
    iface->fptr_recv = welcome_recv;
    iface->fptr_quit = welcome_quit;
    iface->tout      = (struct timeval){ .tv_sec = 0 };
    iface->tofree    = false;

    packet_interface_init(iface);
}
