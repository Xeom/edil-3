#include <stdlib.h>

#include "module/buffers/core.h"
#include "packet.h"
#include "util/log.h"
#include "core.h"

static buffer_s *buffers_get(buffers_ctx_s *ctx, long bn)
{
    buffer_s **bptr;

    bptr = vec_get(&(ctx->buffers), bn);
    if (bptr == NULL) return NULL;

    return *bptr;
}

static long buffers_open(buffers_ctx_s *ctx)
{
    buffer_s *buf;

    buf = malloc(sizeof(buffer_s));
    buffer_init(buf);

    VEC_FOREACH(&(ctx->buffers), buffer_s **, bptr)
    {
        if (*bptr == NULL)
        {
            *bptr = buf;
            return _ind;
        }
    }

    vec_app(&(ctx->buffers), &buf);

    return vec_len(&(ctx->buffers)) - 1;
}

static int buffers_close(buffers_ctx_s *ctx, long bn)
{
    buffer_s *buf, **bptr;

    buf = buffers_get(ctx, bn);
    if (!buf) return -1;

    buffer_kill(buf);
    free(buf);

    bptr = vec_get(&(ctx->buffers), bn);
    *bptr = NULL;

    while (1)
    {
        bptr = vec_last(&(ctx->buffers));
        if (!bptr || *bptr)
            break;

        vec_pop(&(ctx->buffers), 1);
    }

    return 0;
}

static bool buffers_init(packet_interface_s *iface, void **user)
{
    buffers_ctx_s *ctx;
    ctx = malloc(sizeof(buffers_ctx_s));
    *user = ctx;

    vec_init(&(ctx->buffers), sizeof(buffer_s *));

    LOG(INFO, "Buffers module started on iface %p", iface);

    return true;
}

static void buffers_quit(packet_interface_s *iface, void *user)
{
    buffers_ctx_s *ctx;
    ctx = user;
    size_t bn;
    for (bn = 0; bn < vec_len(&(ctx->buffers)); ++bn)
    {
        buffers_close(ctx, bn);
    }

    vec_kill(&(ctx->buffers));
}

static bool buffers_recv(packet_interface_s *iface, void *user, packet_s *pkt, vec_s *data)
{
    buffers_ctx_s *ctx;
    buffer_s *buf;
    ctx = user;

    switch (pkt->type)
    {
    case PACKET_GET_LINE:
        {
            long bn, ln;
            packet_interface_s *rtn;
            bn  = pkt->data.getline.buf;
            ln  = pkt->data.getline.ln;
            rtn = pkt->data.getline.iface;

            packet_s reply = {
                .type = PACKET_SET_LINE,
                .data.setline = {
                    .buf = bn,
                    .ln  = ln
                }
            };

            vec_s chrs;
            vec_init(&chrs, sizeof(chr_s));

            buf = buffers_get(ctx, bn);
            if (buf && buffer_get_line(buf, ln, &chrs) == 0)
            {
                packet_send(rtn, &reply, &chrs);
            }
            else
            {
                /* line does not exist */
            }
        }
        break;

    case PACKET_SET_LINE:
        {
            long bn, ln;
            bn  = pkt->data.setline.buf;
            ln  = pkt->data.setline.ln;

            buf = buffers_get(ctx, bn);
            if (buf && buffer_set_line(buf, ln, data) == 0)
            {
                buffer_send(buf, pkt, data);
            }
        }
        break;

    case PACKET_INS_LINE:
        {
            long bn, ln, n;
            bn  = pkt->data.insline.buf;
            ln  = pkt->data.insline.ln;
            n   = pkt->data.insline.n;

            buf = buffers_get(ctx, bn);
            if (buf && buffer_ins_lines(buf, ln, n) == 0)
            {
                pkt->data.delline.len = buffer_len(buf);
                buffer_send(buf, pkt, data);
            }
        }
        break;

    case PACKET_DEL_LINE:
        {
            long bn, ln, n;
            bn  = pkt->data.delline.buf;
            ln  = pkt->data.delline.ln;
            n   = pkt->data.delline.n;

            buf = buffers_get(ctx, bn);
            if (buf && buffer_del_lines(buf, ln, n) == 0)
            {
                pkt->data.delline.len = buffer_len(buf);
                buffer_send(buf, pkt, data);
            }
        }
        break;

    case PACKET_BUF_OPEN:
        {
            packet_interface_s *sub;
            long bn;
            sub = pkt->data.bufopen.iface;
            bn = buffers_open(ctx);
            buf = buffers_get(ctx, bn);
            buffer_subscribe(buf, sub);

            struct packet_s reply = {
                .type = PACKET_BUF_INFO,
                .data.bufinfo.buf = bn
            };

            LOG(INFO, "Opened new buffer #%ld, subbed to iface %p", bn, iface);
            buffer_send(buf, &reply, NULL);
        }
        break;

    case PACKET_BUF_CLOSE:
        {
            long bn;
            bn  = pkt->data.delline.buf;
            buf = buffers_get(ctx, bn);

            if (!buf) return true;

            buffer_send(buf, pkt, data);

            if (buffers_close(ctx, bn) == 0)
            {
                LOG(INFO, "Closed buffer #%ld", bn);
            }
        }
        break;

    case PACKET_BUF_SUBSCRIBE:
        break;
    }

    return true;
}

void buffers_start(packet_interface_s *iface)
{
    iface->fptr_init = buffers_init;
    iface->fptr_recv = buffers_recv;
    iface->fptr_tout = NULL;
    iface->fptr_quit = buffers_quit;
    iface->tout      = (struct timeval){ 0, 0 };
    iface->tofree    = false;

    packet_interface_init(iface);
}

void buffers_send_get_line(long bn, long ln, packet_interface_s *rtn)
{
    packet_s pkt = {
        .type = PACKET_GET_LINE,
        .data.getline = {
            .buf   = bn,
            .ln    = ln,
            .iface = rtn
        }
    };

    packet_send(&buffers, &pkt, NULL);
}

void buffers_send_set_line(long bn, long ln, vec_s *text)
{
    packet_s pkt = {
        .type = PACKET_SET_LINE,
        .data.setline = {
            .buf = bn,
            .ln  = ln
        }
    };

    packet_send(&buffers, &pkt, text);
}

void buffers_send_ins_line(long bn, long ln, long n)
{
    packet_s pkt = {
        .type = PACKET_INS_LINE,
        .data.insline = {
            .buf = bn,
            .ln  = ln,
            .n   = n
        }
    };

    packet_send(&buffers, &pkt, NULL);
}

void buffers_send_del_line(long bn, long ln, long n)
{
    packet_s pkt = {
        .type = PACKET_DEL_LINE,
        .data.insline = {
            .buf = bn,
            .ln  = ln,
            .n   = n
        }
    };

    packet_send(&buffers, &pkt, NULL);
}

void buffers_send_open(packet_interface_s *sub)
{
    packet_s pkt = {
        .type = PACKET_BUF_OPEN,
        .data.bufopen.iface = sub
    };

    packet_send(&buffers, &pkt, NULL);
}

void buffers_send_close(long bn)
{
    packet_s pkt = {
        .type = PACKET_BUF_CLOSE,
        .data.bufclose.buf = bn
    };

    packet_send(&buffers, &pkt, NULL);
}

void buffers_send_unsubscribe(long bn, packet_interface_s *iface)
{
    packet_s pkt = {
        .type = PACKET_BUF_UNSUBSCRIBE,
        .data.bufsubscribe = {
            .buf   = bn,
            .iface = iface
        }
    };

    packet_send(&buffers, &pkt, NULL);
}

void buffers_send_subscribe(long bn, packet_interface_s *iface)
{
    packet_s pkt = {
        .type = PACKET_BUF_SUBSCRIBE,
        .data.bufsubscribe = {
            .buf   = bn,
            .iface = iface
        }
    };

    packet_send(&buffers, &pkt, NULL);
}
