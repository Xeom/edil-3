#include "module/buffers/line.h"
#include "text/chr.h"

void line_init(line_s *l)
{
    vec_init(&(l->chrs), sizeof(chr_s));
}

void line_kill(line_s *l)
{
    vec_kill(&(l->chrs));
}

void line_set(line_s *l, vec_s *chrs)
{
    vec_clr(&(l->chrs));
    vec_cpy(&(l->chrs), chrs);
}

void line_get(line_s *l, vec_s *chrs)
{
    vec_clr(chrs);
    vec_cpy(chrs, &(l->chrs));
}
