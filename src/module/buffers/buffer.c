#include "module/buffers/line.h"

#include "module/buffers/buffer.h"

void buffer_init(buffer_s *buf)
{
    vec_init(&(buf->lines),      sizeof(line_s));
    vec_init(&(buf->subscribed), sizeof(packet_interface_s));
}

void buffer_kill(buffer_s *buf)
{
    VEC_FOREACH(&(buf->lines), line_s *, l)
    {
        line_kill(l);
    }

    vec_kill(&(buf->lines));
}

int buffer_get_line(buffer_s *buf, long ln, vec_s *chrs)
{
    line_s *l;

    l = vec_get(&(buf->lines), ln);
    if (!l) return -1;

    line_get(l, chrs);

    return 0;
}

int buffer_set_line(buffer_s *buf, long ln, vec_s *chrs)
{
    line_s *l;

    l = vec_get(&(buf->lines), ln);
    if (!l) return -1;

    line_set(l, chrs);

    return 0;
}

int buffer_ins_lines(buffer_s *buf, long ln, long n)
{
    if (n <= 0)
        return -1;

    if (vec_ins(&(buf->lines), ln, n, NULL) == NULL)
        return -1;

    for (; n > 0; --n, ++ln)
    {
        line_s *l;

        l = vec_get(&(buf->lines), ln);
        if (!l) return -1;

        line_init(l);
    }

    return 0;
}

int buffer_del_lines(buffer_s *buf, long ln, long n)
{
    if (n <= 0)
        return -1;

    for (; n > 0; --n, ++ln)
    {
        line_s *l;

        l = vec_get(&(buf->lines), ln);
        if (!l) continue;

        line_kill(l);
    }

    if (vec_del(&(buf->lines), ln, n) == -1)
        return -1;

    return 0;
}

int buffer_subscribe(buffer_s *buf, packet_interface_s *iface)
{
    vec_s *subs;
    subs = &(buf->subscribed);

    VEC_FOREACH(subs, packet_interface_s **, cmp)
    {
        if (*cmp == iface)
            return -1;
    }

    vec_app(subs, &iface);

    return 0;
}

int buffer_unsubscribe(buffer_s *buf, packet_interface_s *iface)
{
    vec_s *subs;
    subs = &(buf->subscribed);

    VEC_FOREACH(subs, packet_interface_s **, cmp)
    {
        if (*cmp == iface)
            return vec_del(subs, _ind, 1);
    }

    return -1;
}

int buffer_send(buffer_s *buf, packet_s *pkt, vec_s *data)
{
    vec_s *subs;
    subs = &(buf->subscribed);

    VEC_FOREACH(subs, packet_interface_s **, iface)
    {
        packet_send(*iface, pkt, data);
    }

    return 0;
}

size_t buffer_len(buffer_s *buf)
{
    return vec_len(&(buf->lines));
}
