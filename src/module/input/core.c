#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#include "packet.h"
#include "util/log.h"
#include "module/input/parser.h"
#include "module/input/keys.h"

#include "module/input/core.h"

const int input_thread_tout      = 1000;
const int input_thread_tout_long = 100000;

static void *input_parser_thread_main(void *arg)
{
    fd_set fds;
    input_ctx_s *ctx = arg;
    struct timeval tout = { .tv_usec = input_thread_tout };

    LOG(INFO, "Beginning parser thread");

    FD_ZERO(&fds);

    while (ctx->alive)
    {
        FD_SET(ctx->fd, &fds);
        select(ctx->fd + 1, &fds, NULL, NULL, &tout);

        if (FD_ISSET(ctx->fd, &fds))
        {
            char c;
            if (read(ctx->fd, &c, sizeof(char)) != 1)
                continue;

            input_parse(&(ctx->parser), c);
            tout.tv_usec = input_thread_tout;
        }
        else
        {
            input_parser_flush_str(&(ctx->parser));
            tout.tv_usec = input_thread_tout_long;
        }
    }

    LOG(INFO, "Killed parser thread");

    return NULL;
}

static void input_stop_parser(input_ctx_s *ctx)
{
    ctx->alive = false;
    pthread_join(ctx->thread, NULL);
}

static bool input_init(packet_interface_s *iface, void **user)
{
    input_ctx_s *ctx;
    ctx = malloc(sizeof(input_ctx_s));
    *user = ctx;

    memset(ctx, 0, sizeof(input_ctx_s));

    input_parser_ctx_init(&(ctx->parser));
    input_keys_add(&(ctx->parser));

    ctx->fd = STDIN_FILENO;
    ctx->alive = true;

    pthread_create(&(ctx->thread), NULL, input_parser_thread_main, ctx);

    LOG(INFO, "Input started on iface %p", (void *)iface);

    return true;
}

static void input_quit(packet_interface_s *iface, void *user)
{
    input_ctx_s *ctx;
    ctx = user;

    input_stop_parser(ctx);
    input_parser_ctx_kill(&(ctx->parser));

    free(ctx);
}

void input_start(packet_interface_s *iface)
{
    memset(iface, 0, sizeof(packet_interface_s));
    iface->fptr_init = input_init;
    iface->fptr_quit = input_quit;

    packet_interface_init(iface);
}
