#include <stdlib.h>
#include <string.h>

#include "text/chr.h"
#include "util/log.h"
#include "packet.h"
#include "core.h"

#include "module/input/parser.h"

static void input_parse_mouseclick(input_parser_ctx_s *ctx, unsigned char c)
{
    ctx->seq[(ctx->seqind)++] = c;

    if (ctx->seqind == 3)
    {
        int x, y, button;
        ctx->seqind = 0;
        ctx->mouseclick = false;

        button = ctx->seq[0] - 32;
        x      = ctx->seq[1] - 32;
        y      = ctx->seq[2] - 32;

        packet_s pkt =
        {
            .type = PACKET_MOUSE_PRESS,
            .data.mousepress =
            {
                .x = x,
                .y = y,
                .button = button
            }
        };

        LOG(KEYS, "Mouseclick at %d %d, button %d", x, y, button);

        packet_send(&display, &pkt, NULL);
    }
}


static void input_parse_str(input_parser_ctx_s *ctx, char c)
{
    if (chr_decode(&(ctx->chr), c))
    {
        vec_app(&(ctx->chrs), &(ctx->chr));
        memset(&(ctx->chr), 0, sizeof(chr_s));
    }
}

static void input_parse_key(input_parser_ctx_s *ctx, input_key_t key)
{
    if (ctx->escaped)
    {
        key |= KEY_ESC;
        ctx->escaped = false;
    }

    if (key <= 0xff)
        input_parse_str(ctx, (char)key);
    else if (key == KEY_MOUSECLICK)
        ctx->mouseclick = true;
    else
    {
        packet_s pkt =
        {
            .type = PACKET_KEY_PRESS,
            .data.keypress.key = key
        };

        input_parser_flush_str(ctx);
        LOG(KEYS, "Keypress %03x", key);

        packet_send(&display, &pkt, NULL);
    }
}

static void input_parse_sing(input_parser_ctx_s *ctx, unsigned char c)
{
    if (c == '\033')
    {
        ctx->escaped = true;
        return;
    }

    if (c < 0x20)
        input_parse_key(ctx, KEY_CTRL | (c + 0x40));
    else
        input_parse_key(ctx, c);
}

void input_parser_flush_str(input_parser_ctx_s *ctx)
{
    packet_s pkt = { .type = PACKET_TYPED };

    if (vec_len(&(ctx->chrs)) == 0)
        return;

    LOG(KEYS, "Recieved string of %lu characters", vec_len(&(ctx->chrs)));
    packet_send(&display, &pkt, &(ctx->chrs));

    vec_clr(&(ctx->chrs));
}

void input_parse(input_parser_ctx_s *ctx, unsigned char c)
{
    input_key_t key;
    input_kmap_s *next;

    key  = ctx->current->keys[c];
    next = ctx->current->subs[c];

    if (ctx->mouseclick)
    {
        input_parse_mouseclick(ctx, c);
    }
    else if (key != KEY_NONE)
    {
        input_parse_key(ctx, key);

        ctx->current = &ctx->root;
        ctx->seqind  = 0;
    }
    else if (next != NULL)
    {
        ctx->current = next;
        ctx->seq[ctx->seqind] = c;
        ctx->seqind += 1;
    }
    else if (ctx->current != &ctx->root)
    {
        int ind;

        for (ind = 0; ind < ctx->seqind; ++ind)
            input_parse_sing(ctx, ctx->seq[ind]);

        ctx->current = &ctx->root;
        ctx->seqind  = 0;

        input_parse(ctx, c);
    }
    else
    {
        input_parse_sing(ctx, c);
    }
}

static void input_kmap_init(input_kmap_s *kmap)
{
    int c;
    for (c = 0; c < 256; ++c)
    {
        kmap->keys[c] = KEY_NONE;
        kmap->subs[c] = NULL;
    }
}

static void input_kmap_kill(input_parser_ctx_s *ctx, input_kmap_s *kmap)
{
    int c;
    for (c = 0; c < 256; ++c)
    {
        if (kmap->subs[c])
            input_kmap_kill(ctx, kmap->subs[c]);
    }

    if (kmap != &(ctx->root))
        free(kmap);
}

void input_parser_ctx_init(input_parser_ctx_s *ctx)
{
    memset(ctx, 0, sizeof(input_parser_ctx_s));
    input_kmap_init(&(ctx->root));
    vec_init(&(ctx->chrs), sizeof(chr_s));
    ctx->current = &(ctx->root);
}

void input_parser_ctx_kill(input_parser_ctx_s *ctx)
{
    input_kmap_kill(ctx, &(ctx->root));
    vec_kill(&(ctx->chrs));
}

void input_parser_add_key(input_parser_ctx_s *ctx, char *combo, input_key_t key)
{
    input_kmap_s *kmap;

    kmap = &ctx->root;

    unsigned char c;
    while ((c = (unsigned char)*(combo++)))
    {
        /* If this is the last character */
        if (!(*combo))
        {
            kmap->keys[c] = key;
        }
        else if (!kmap->subs[c])
        {
            input_kmap_s *new;

            new = malloc(sizeof(input_kmap_s));

            input_kmap_init(new);

            kmap->subs[c] = new;
            kmap = new;
        }
        else
        {
            kmap = kmap->subs[c];
        }
    }
}
