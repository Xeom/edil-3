#include "module/input/keys.h"

void input_keys_add(input_parser_ctx_s *ctx)
{
    input_parser_add_key(ctx, "\n",   KEY_ENTER);
    input_parser_add_key(ctx, "\t",   KEY_TAB);
    input_parser_add_key(ctx, "\177", KEY_BACK);
    input_parser_add_key(ctx, "\b",   KEY_BACK | KEY_CTRL);

    input_parser_add_key(ctx, "\033[A", KEY_UP);
    input_parser_add_key(ctx, "\033[B", KEY_DOWN);
    input_parser_add_key(ctx, "\033[C", KEY_RIGHT);
    input_parser_add_key(ctx, "\033[D", KEY_LEFT);

    input_parser_add_key(ctx, "\033[1~", KEY_HOME);
    input_parser_add_key(ctx, "\033[3~", KEY_DEL);
    input_parser_add_key(ctx, "\033[P",  KEY_DEL);
    input_parser_add_key(ctx, "\033[4~", KEY_END);
    input_parser_add_key(ctx, "\033[5~", KEY_PGUP);
    input_parser_add_key(ctx, "\033[6~", KEY_PGDN);
    input_parser_add_key(ctx, "\033[4h", KEY_INS);
    input_parser_add_key(ctx, "\033[Z",  KEY_SHIFTTAB);
    input_parser_add_key(ctx, "\033[2K", KEY_SHIFTDEL);

    input_parser_add_key(ctx, "\033[200~", KEY_PASTEBEGIN);
    input_parser_add_key(ctx, "\033[201~", KEY_PASTEEND);

    input_parser_add_key(ctx, "\033OA", KEY_CTRL | KEY_UP);
    input_parser_add_key(ctx, "\033OB", KEY_CTRL | KEY_DOWN);
    input_parser_add_key(ctx, "\033OC", KEY_CTRL | KEY_RIGHT);
    input_parser_add_key(ctx, "\033OD", KEY_CTRL | KEY_LEFT);
    input_parser_add_key(ctx, "\033[J", KEY_CTRL | KEY_END);

    input_parser_add_key(ctx, "\033OP",   KEY_F1);
    input_parser_add_key(ctx, "\033OQ",   KEY_F2);
    input_parser_add_key(ctx, "\033OR",   KEY_F3);
    input_parser_add_key(ctx, "\033OS",   KEY_F4);
    input_parser_add_key(ctx, "\033[15~", KEY_F5);
    input_parser_add_key(ctx, "\033[17~", KEY_F6);
    input_parser_add_key(ctx, "\033[18~", KEY_F7);
    input_parser_add_key(ctx, "\033[19~", KEY_F8);
    input_parser_add_key(ctx, "\033[20~", KEY_F9);
    input_parser_add_key(ctx, "\033[21~", KEY_F10);
    input_parser_add_key(ctx, "\033[23~", KEY_F11);
    input_parser_add_key(ctx, "\033[24~", KEY_F12);

    input_parser_add_key(ctx, "\033[M", KEY_MOUSECLICK);
}
