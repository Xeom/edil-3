#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include "packet.h"
#include "util/log.h"
#include "container/vec.h"

#include "module/splitter/core.h"

static bool splitter_init(packet_interface_s *iface, void **user)
{
    *user = malloc(sizeof(vec_s));
    vec_init(*user, sizeof(packet_interface_s *));

    LOG(INFO, "Splitter started on iface %p", (void *)iface);

    return true;
}

static void splitter_quit(packet_interface_s *iface, void *user)
{
    vec_kill(user);
    free(user);
}

static bool splitter_recv(
    packet_interface_s *iface, void *user, packet_s *pkt, vec_s *data
)
{
    vec_s *dests;
    dests = user;

    switch (pkt->type)
    {
    case PACKET_ADD_DEST:
        {
            LOG(INFO, "Splitter %p adding %lu new destination(s)",
                (void *)iface, vec_len(data));
            VEC_FOREACH(data, packet_interface_s **, new)
            {
                LOG(INFO, " -> added %p as new dest", *new);
                vec_app(dests, new);
            }

        }
        break;

    case PACKET_DEL_DEST:
        {
            LOG(INFO, "Splitter %p removing %lu destination(s)",
                (void *)iface, vec_len(data));
            VEC_FOREACH(data, packet_interface_s **, del)
            {
                VEC_FOREACH(dests, packet_interface_s **, cmp)
                {
                    if (*cmp == *del)
                    {
                        LOG(INFO, " -> removed %p as dest", *del);
                        break;
                    }
                }
            }
        }
        break;


    default:
        {
            VEC_FOREACH(dests, packet_interface_s **, sendto)
            {
                packet_send(*sendto, pkt, data);
            }
        }
        break;
    }

    return true;
}

void splitter_start(packet_interface_s *iface)
{
    iface->fptr_init = splitter_init;
    iface->fptr_recv = splitter_recv;
    iface->fptr_tout = NULL;
    iface->fptr_quit = splitter_quit;
    iface->tout      = (struct timeval){ .tv_sec = 0 };
    iface->tofree    = false;

    packet_interface_init(iface);
}

void splitter_add_dest(
    packet_interface_s *iface, packet_interface_s *dest
)
{
    vec_s    data;
    packet_s pkt = { .type = PACKET_ADD_DEST };

    vec_init(&data, sizeof(packet_interface_s *));
    vec_app(&data, &dest);

    packet_send(iface, &pkt, &data);

    vec_kill(&data);
}

void splitter_del_dest(
    packet_interface_s *iface, packet_interface_s *dest
)
{
    vec_s    data;
    packet_s pkt = { .type = PACKET_DEL_DEST };

    vec_init(&data, sizeof(packet_interface_s *));
    vec_app(&data, &dest);

    packet_send(iface, &pkt, &data);

    vec_kill(&data);
}
