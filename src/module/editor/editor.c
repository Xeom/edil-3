#include <stdlib.h>
#include "util/log.h"
#include "core.h"
#include "module/display/core.h"
#include "module/splitter/core.h"

#include "module/editor/editor.h"

static chr_s editor_default_marker   = { .utf8 = " " };
static chr_s editor_primary_marker   = { .utf8 = "~" };
static chr_s editor_secondary_marker = { .utf8 = "-" };
static int   editor_primary_interval   = 50;
static int   editor_secondary_interval = 10;

void editor_request(editor_ctx_s *ctx, packet_interface_s *iface, long ln)
{
    buffers_send_get_line(ctx->buf, ln, iface);
}

void editor_request_after(editor_ctx_s *ctx, packet_interface_s *iface, long ln)
{
    long end;
    end = ctx->rows + ctx->scry;

    if (ln < ctx->scry)
        ln = ctx->scry;

    for (; ln < end; ++ln)
    {
        editor_request(ctx, iface, ln);
    }
}

chr_s *editor_get_line_marker(editor_ctx_s *ctx, long ln)
{
    if (ln % editor_primary_interval == 0)
        return &editor_primary_marker;
    else if (ln % editor_secondary_interval == 0)
        return &editor_secondary_marker;
    else
        return &editor_default_marker;
}

void editor_handle_update(editor_ctx_s *ctx, long buf, long ln, vec_s *chrs)
{
    long x, y;

    x = 0;
    y = ln - ctx->scry;

    if (ctx->win == -1)               return;
    if (buf != ctx->buf)              return;
    if (ln < ctx->scry)               return;
    if (ln >= ctx->scry + ctx->rows)  return;

    if (ctx->scrx > (long)vec_len(chrs) || ctx->scrx < -(long)vec_len(chrs))
        vec_clr(chrs);
    else if (ctx->scrx < 0)
        x = -ctx->scrx;
    else if (ctx->scrx > 0)
        vec_del(chrs, 0, ctx->scrx);

    vec_ins(chrs, 0, 1, editor_get_line_marker(ctx, ln));

    if (vec_len(chrs) > (size_t)ctx->cols)
        vec_pop(chrs, vec_len(chrs) - ctx->cols);

    display_send_clear_at(&display, 0, y, ctx->win);
    display_send_chrs(&display, x, y, ctx->win, chrs);
}
