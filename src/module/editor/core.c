#include <stdlib.h>
#include "util/log.h"
#include "core.h"
#include "module/display/core.h"
#include "module/splitter/core.h"
#include "module/editor/editor.h"

#include "module/editor/core.h"

static bool editor_init(packet_interface_s *iface, void **user)
{
    editor_ctx_s *ctx;
    ctx = malloc(sizeof(editor_ctx_s));
    *user = ctx;

    ctx->win = -1;
    ctx->buf = -1;
    ctx->scrx = 0;
    ctx->scry = 0;
    ctx->cols = 10;
    ctx->rows = 10;

    LOG(INFO, "Viewer started on iface %p", iface);

    return true;
}

static void editor_quit(packet_interface_s *iface, void *user)
{
    editor_ctx_s *ctx;
    ctx = user;

    buffers_send_unsubscribe(ctx->buf, iface);
    LOG(INFO, "Viewer exited");
}

static bool editor_recv(packet_interface_s *iface, void *user, packet_s *pkt, vec_s *data)
{
    editor_ctx_s *ctx;
    ctx = user;

    switch (pkt->type)
    {
    case PACKET_BUF_INFO:
        {
            long buf;
            buf = pkt->data.bufinfo.buf;
            ctx->buf = buf;
        }
        break;

    case PACKET_SET_LINE:
        {
            long buf, ln;
            buf = pkt->data.setline.buf;
            ln  = pkt->data.setline.ln;

            editor_handle_update(ctx, buf, ln, data);
        }
        break;

    case PACKET_INS_LINE:
        {
            long buf, ln;
            buf = pkt->data.insline.buf;
            ln  = pkt->data.insline.ln;

            if (buf != ctx->buf) return true;

            editor_request_after(ctx, iface, ln);
        }
        break;

    case PACKET_DEL_LINE:
        {
            long buf, ln;
            buf = pkt->data.delline.buf;
            ln  = pkt->data.delline.ln;

            if (buf != ctx->buf) return true;

            editor_request_after(ctx, iface, ln);
        }
        break;

    case PACKET_WIN_INFO:
        {
            ctx->win  = pkt->data.wininfo.win;
            ctx->cols = pkt->data.wininfo.cols;
            ctx->rows = pkt->data.wininfo.rows;

            LOG(INFO, "Viewer window updated - #%d, %dx%d",
                ctx->win, ctx->cols, ctx->rows);

            editor_request_after(ctx, iface, 0);
        }
        break;

    case PACKET_WIN_CLOSE:
        {
            int win;

            win = pkt->data.winclose.win;

            LOG(INFO, "Viewer window on iface %p recieved window %d close", iface, win);
            if (win == ctx->win)
                return false;
        }
        break;
    }

    return true;
}

void editor_start(packet_interface_s *iface)
{
    iface->fptr_init = editor_init;
    iface->fptr_recv = editor_recv;
    iface->fptr_tout = NULL;
    iface->fptr_quit = editor_quit;
    iface->tout      = (struct timeval){0, 0};
    iface->tofree    = false;

    packet_interface_init(iface);
}
