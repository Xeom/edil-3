struct viewer_cache_s
{
    long off;
    vec_s lines;
}

struct viewer_line_s
{
    bool  here;
    vec_s chrs;
}
