#include <signal.h>
#include <stdlib.h>
#include <stdarg.h>

#include "text/chr.h"
#include "util/log.h"
#include "util/sys.h"
#include "util/term.h"

#include "module/display/frame.h"
#include "module/display/display.h"
#include "module/display/core.h"

static void *display_winch_wait(void *arg)
{
    display_ctx_s *d;
    d = arg;

    LOG(INFO, "Starting SIGWINCH listening thread");

    while (1)
    {
        sys_sig_wait(SIGWINCH);
        LOG(INFO, "SIGWINCH recieved");
        d->winch = true;
    }

    return NULL;
}

static bool display_init(packet_interface_s *iface, void **user)
{
    display_ctx_s *d;
    d = malloc(sizeof(display_ctx_s));
    *user = d;

    d->rows = 30;
    d->cols = 30;

    d->winch = true;

    d->root = malloc(sizeof(frame_s));
    memset(d->root, 0, sizeof(frame_s));

    d->root->dir    = WIN_SPLIT_NONE;
    d->root->adj    = 0.5f;
    d->sel          = 0;

    term_init();
    term_get_dims(&(d->cols), &(d->rows));

    vec_init(&(d->frames), sizeof(frame_s *));
    display_add_frame(d, d->root);

    pthread_create(
        &(d->winch_thread),
        NULL,
        display_winch_wait,
        d
    );

    pthread_detach(d->winch_thread);

    LOG(INFO, "Display started on iface %p", iface);

    return true;
}

static void display_quit(packet_interface_s *iface, void *user)
{
    packet_s closepkt;
    display_ctx_s *d;
    d = user;

    memset(&closepkt, 0, sizeof(packet_s));
    closepkt.type = PACKET_WIN_CLOSE;

    pthread_cancel(d->winch_thread);

    VEC_RFOREACH(&(d->frames), frame_s **, frm)
    {
        if (frm == NULL || *frm == NULL) continue;

        closepkt.data.winclose.win = _ind;

        if ((*frm)->win)
            packet_send((*frm)->win, &closepkt, NULL);

        display_close(d, _ind);
    }

    display_kill(d);
    free(user);

    term_kill();

    LOG(INFO, "Display exited");
}

static bool display_tout(packet_interface_s *iface, void *user)
{
    display_ctx_s *d;
    d = user;

    if (d->winch)
    {
        LOG(INFO, "Resizing display");
        term_get_dims(&(d->cols), &(d->rows));
        frame_update_dims(d->root, d);
        printf(TERM_CLR_SCREEN);
        display_print(d);
        d->winch = false;
        display_send_info_all(d);
    }

    return true;
}

static bool display_recv(packet_interface_s *iface, void *user, packet_s *pkt, vec_s *data)
{
    display_ctx_s *d;
    d = user;

    switch (pkt->type)
    {
    case PACKET_WIN_SPLIT:
        {
            int win, dir;
            packet_interface_s *dest;

            win  = pkt->data.winsplit.win;
            dir  = pkt->data.winsplit.dir;
            dest = pkt->data.winsplit.iface;

            display_split(d, win, dir, dest);
        }
        break;

    case PACKET_WIN_CLOSE:
        {
            frame_s *frm;

            int win = pkt->data.winclose.win;

            frm = display_get_frame(d, win);
            if (!frm) break;

            if (frm->win)
                packet_send(frm->win, pkt, data);

            display_close(d, win);
        }
        break;

    case PACKET_WIN_IFACE:
        {
            int win;
            packet_interface_s *dest;

            win  = pkt->data.winiface.win;
            dest = pkt->data.winiface.iface;

            display_set_iface(d, win, dest);
        }
        break;

    case PACKET_WIN_TEXT:
        {
            int win, x, y;

            win = pkt->data.wintext.win;
            x   = pkt->data.wintext.x;
            y   = pkt->data.wintext.y;

            display_print_content(d, win, x, y, data);
        }
        break;

    case PACKET_WIN_CLEAR:
        {
            int win;

            win = pkt->data.winclear.win;

            display_clear(d, win);
        }
        break;

    case PACKET_WIN_CLEAR_AT:
        {
            int win, x, y;

            win = pkt->data.winclearat.win;
            x   = pkt->data.winclearat.x;
            y   = pkt->data.winclearat.y;

            display_clear_at(d, win, x, y);
        }
        break;

    case PACKET_WIN_SEL:
        {
            int win;

            win = pkt->data.winsel.win;

            d->sel = win;

            display_print(d);
        }
        break;

    case PACKET_KEY_PRESS:
    case PACKET_TYPED:
    case PACKET_MOUSE_PRESS:
        {
            frame_s *sel;
            sel = display_get_selected(d);
            if (!sel) break;

            if (sel->win)
                packet_send(sel->win, pkt, data);
        }
        break;
    }

    return true;
}

void display_start(packet_interface_s *iface)
{
    iface->fptr_init = display_init;
    iface->fptr_recv = display_recv;
    iface->fptr_tout = display_tout;
    iface->fptr_quit = display_quit;
    iface->tout      = (struct timeval){ .tv_usec = 50000 };
    iface->tofree    = false;

    packet_interface_init(iface);
}

void display_send_chrs(packet_interface_s *iface, int x, int y, int win, vec_s *text)
{
    packet_s pkt = {
        .type = PACKET_WIN_TEXT,
        .data.wintext = {
            .win = win,
            .x = x,
            .y = y
        }
    };

    packet_send(iface, &pkt, text);
}

void display_send_format(packet_interface_s *iface, int x, int y, int win, char *fmt, ...)
{
    va_list args;
    vec_s text;
    vec_init(&text, sizeof(chr_s));

    va_start(args, fmt);
    chr_va_format(&text, fmt, args);
    va_end(args);

    display_send_chrs(iface, x, y, win, &text);

    vec_kill(&text);
}

void display_send_str(packet_interface_s *iface, int x, int y, int win, char *str)
{
    vec_s text;
    vec_init(&text, sizeof(chr_s));
    chr_from_str(&text, str);

    display_send_chrs(iface, x, y, win, &text);

    vec_kill(&text);
}

void display_send_clear(packet_interface_s *iface, int win)
{
    packet_s pkt = {
        .type = PACKET_WIN_CLEAR,
        .data.winclear.win = win
    };

    packet_send(iface, &pkt, NULL);
}

void display_send_clear_at(packet_interface_s *iface, int x, int y, int win)
{
    packet_s pkt = {
        .type = PACKET_WIN_CLEAR_AT,
        .data.winclearat = {
            .win = win,
            .x   = x,
            .y   = y
        }
    };

    packet_send(iface, &pkt, NULL);
}

void display_send_split(
    packet_interface_s *iface, int win, frame_split_t dir, packet_interface_s *winiface)
{
    packet_s pkt = {
        .type = PACKET_WIN_SPLIT,
        .data.winsplit = {
            .win = win,
            .dir = dir,
            .iface = winiface
        }
    };

    packet_send(iface, &pkt, NULL);
}

void display_send_win_iface(
    packet_interface_s *iface, int win, packet_interface_s *winiface)
{
    packet_s pkt = {
        .type = PACKET_WIN_IFACE,
        .data.winiface = {
            .win = win,
            .iface = winiface
        }
    };

    packet_send(iface, &pkt, NULL);
}

void display_send_close(packet_interface_s *iface, int win)
{
    packet_s pkt = {
        .type = PACKET_WIN_CLOSE,
        .data.winclose.win = win
    };

    packet_send(iface, &pkt, NULL);
}
