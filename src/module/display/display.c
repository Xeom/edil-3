#include <stdio.h>
#include <stdlib.h>

#include "util/log.h"
#include "text/col.h"
#include "util/term.h"
#include "util/sys.h"

#include "module/display/display.h"
#include "module/display/frame.h"

frame_s *display_get_frame(display_ctx_s *d, int id)
{
    frame_s **rtn;

    rtn = vec_get(&(d->frames), id);

    if (!rtn) return NULL;

    return *rtn;
}

frame_s *display_get_selected(display_ctx_s *d)
{
    return display_get_frame(d, d->sel);
}

void display_print(display_ctx_s *d)
{
    printf(TERM_HIDE_CUR);

    VEC_FOREACH(&(d->frames), frame_s **, frm)
    {
        if (!(*frm)) continue;

        if ((int)_ind == d->sel)
            col_print(COL_WIN_FRAME_SEL);
        else
            col_print(COL_WIN_FRAME);

        frame_print(*frm, d);

        col_print(COL_DEFAULT);
    }

    frame_reset_cursor(display_get_selected(d));
}

int display_add_frame(display_ctx_s *d, frame_s *frm)
{
    VEC_FOREACH(&(d->frames), frame_s **, iterfrm)
    {
        if (*iterfrm == NULL)
        {
            *iterfrm = frm;
            return _ind;
        }
    }

    vec_app(&(d->frames), &frm);

    return vec_len(&(d->frames)) - 1;
}

void display_del_frame(display_ctx_s *d, int id)
{
    frame_s **frm;

    frm  = vec_get(&(d->frames), id);
    *frm = NULL;

    while (vec_len(&(d->frames)) &&
        *(frame_s **)vec_last(&(d->frames)) == NULL)
    {
        vec_pop(&(d->frames), 1);
    }
}

int display_split(
    display_ctx_s *d, int id, frame_split_t dir, packet_interface_s *iface)
{
    frame_s *frm, *new;

    if ((frm = display_get_frame(d, id)) == NULL)
        return -1;

    new = frame_split(frm, d, dir);
    if (!new) return -1;

    new->win = iface;

    display_add_frame(d, new);
    printf(TERM_CLR_SCREEN);
    display_print(d);

    display_send_info_all(d);

    return 0;
}

int display_close(display_ctx_s *d, int id)
{
    int newsel;
    frame_s *frm;

    if ((frm = display_get_frame(d, id)) == NULL)
        return -1;

    if (frame_close(frm, d) == -1)
        return -1;

    display_del_frame(d, id);
    printf(TERM_CLR_SCREEN);
    display_print(d);

    /* If the window closed was the selected one, *
     * we find a new one to select!               */
    if (id == d->sel)
    {
        newsel = d->sel + 1;
        while (newsel != d->sel)
        {
            if (display_get_frame(d, newsel))
            {
                d->sel = newsel;
                break;
            }

            newsel--;
            if (newsel < 0)
                newsel = vec_len(&(d->frames)) - 1;
        }
    }

    display_send_info_all(d);

    return 0;
}

int display_clear(display_ctx_s *d, int id)
{
    frame_s *frm;

    if ((frm = display_get_frame(d, id)) == NULL)
        return -1;

    frame_clear(frm, d);

    return 0;
}

int display_clear_at(display_ctx_s *d, int id, int x, int y)
{
    frame_s *frm;

    if ((frm = display_get_frame(d, id)) == NULL)
        return -1;

    frame_clear_at(frm, d, x, y);

    return 0;
}

int display_set_iface(display_ctx_s *d, int id, packet_interface_s *iface)
{
    frame_s *frm;
    packet_s pkt = {
        .type = PACKET_WIN_CLOSE,
        .data.winclose.win = id
    };

    LOG(INFO, "Display setting window #%d to iface %p", id, iface);

    if ((frm = display_get_frame(d, id)) == NULL)
        return -1;

    if (frm->win)
        packet_send(frm->win, &pkt, NULL);

    frm->win = iface;
    frame_send_info(frm, id);

    return 0;
}

int display_send_info(display_ctx_s *d, int id)
{
    frame_s *frm;

    if ((frm = display_get_frame(d, id)) == NULL)
        return -1;

    frame_send_info(frm, id);

    return 0;
}

void display_send_info_all(display_ctx_s *d)
{
    VEC_FOREACH(&(d->frames), frame_s **, frm)
    {
        if (*frm == NULL) continue;
        frame_send_info(*frm, _ind);
    }
}

int display_print_content(display_ctx_s *d, int id, int x, int y, vec_s *chrs)
{
    frame_s *frm;

    printf(TERM_HIDE_CUR);

    if ((frm = display_get_frame(d, id)) == NULL)
        return -1;

    frame_print_content(frm, x, y, chrs);

    frame_reset_cursor(display_get_selected(d));

    return 0;
}

void display_kill(display_ctx_s *d)
{
    frame_free(d->root);
    vec_kill(&(d->frames));
}
