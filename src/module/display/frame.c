#include <signal.h>
#include <stdlib.h>
#include <stdio.h>

#include "text/chr.h"
#include "util/sys.h"
#include "util/term.h"

#include "module/display/frame.h"

static inline bool frame_is_child1(frame_s *frm)
{
    return (frm->parent) && (frm == frm->parent->child1);
}

static inline bool frame_is_child2(frame_s *frm)
{
    return (frm->parent) && (frm == frm->parent->child2);
}

static inline bool frame_is_root(frame_s *frm)
{
    return (frm->parent == NULL);
}

static inline frame_s *frame_sister(frame_s *frm)
{
    if (frame_is_child1(frm))
        return frm->parent->child2;
    else if (frame_is_child2(frm))
        return frm->parent->child1;
    else
        return NULL;
}

static void frame_get_dims(frame_s *frm, display_ctx_s *d, frame_dims_s *dims)
{
    float adj;
    frame_split_t dir;

    if (frm == NULL) return;

    if (frame_is_root(frm))
    {
        dims->x = 0;
        dims->y = 0;
        dims->w = d->cols + 1;
        dims->h = d->rows;

        return;
    }

    memcpy(dims, &(frm->parent->dims), sizeof(frame_dims_s));

    adj = frm->parent->adj;
    dir = frm->parent->dir;

    if (frame_is_child1(frm))
    {
        if (dir == WIN_SPLIT_HORIZONTAL)
            dims->h = dims->h * adj;
        else if (dir == WIN_SPLIT_VERTICAL)
            dims->w = dims->w * adj;
    }
    else if (frame_is_child2(frm))
    {
        if (dir == WIN_SPLIT_HORIZONTAL)
        {
            dims->y += dims->h * adj;
            dims->h -= dims->h * adj;
        }
        else if (dir == WIN_SPLIT_VERTICAL)
        {
            dims->x += dims->w * adj;
            dims->w -= dims->w * adj;
        }
    }

    if (dims->w < 0)       dims->w = 0;
    if (dims->h < 0)       dims->h = 0;
    if (dims->x < 0)       dims->x = 0;
    if (dims->y < 0)       dims->y = 0;
    if (dims->x > d->cols) dims->x = d->cols;
    if (dims->y > d->rows) dims->y = d->rows;
}

void frame_update_dims(frame_s *frm, display_ctx_s *d)
{
    if (!frm) return;

    frame_get_dims(frm, d, &(frm->dims));

    frame_update_dims(frm->child1, d);
    frame_update_dims(frm->child2, d);
}

frame_s *frame_split(frame_s *frm, display_ctx_s *d, frame_split_t dir)
{
    frame_s *new, *newsub;

    newsub = malloc(sizeof(frame_s));
    memset(newsub, 0, sizeof(frame_s));
    newsub->dir = WIN_SPLIT_NONE;
    newsub->adj = 0.5f;

    new = malloc(sizeof(frame_s));
    memset(newsub, 0, sizeof(frame_s));
    new->dir = dir;
    new->adj = 0.5f;
    new->parent = frm->parent;
    new->child1 = frm;
    new->child2 = newsub;

    if (frame_is_child1(frm))
        frm->parent->child1 = new;
    else if (frame_is_child2(frm))
        frm->parent->child2 = new;

    newsub->parent = new;
    frm->parent    = new;
    if (frame_is_root(new))
        d->root = new;

    frame_update_dims(new, d);

    return newsub;
}

void frame_free(frame_s *frm)
{
    if (frm->child1 && frm->child2)
    {
        frame_free(frm->child1);
        frame_free(frm->child2);
    }

    free(frm);
}

int frame_close(frame_s *frm, display_ctx_s *d)
{
    frame_s *sister, *parent;

    if (!frm->parent) return -1;

    sister = frame_sister(frm);
    parent = frm->parent;
    sister->parent = parent->parent;

    if (frame_is_child1(parent))
        parent->parent->child1 = sister;
    else if (frame_is_child2(parent))
        parent->parent->child2 = sister;

    if (frame_is_root(sister))
        d->root = sister;

    parent->child1 = NULL;
    parent->child2 = NULL;
    frame_free(parent);
    frame_free(frm);

    frame_update_dims(sister, d);

    return 0;
}

void frame_print(frame_s *frm, display_ctx_s *d)
{
    int off;
    frame_dims_s *dims;
    dims = &(frm->dims);

    if (dims->x + dims->w < d->cols)
    {
        term_goto(dims->x + dims->w, dims->y + 1);
        for (off = 0; off < dims->h; ++off)
            printf("|" TERM_DOWN TERM_LEFT);
    }

    term_goto(dims->x + 1, dims->y + dims->h);
    for (off = 0; off < dims->w && off + dims->x < d->cols; ++off)
        printf("-");

    fflush(stdout);
}

void frame_print_content(frame_s *frm, int x, int y, vec_s *chrs)
{
    int len, ind, maxind;
    frame_dims_s *dims;
    dims = &(frm->dims);

    len = vec_len(chrs);
    ind = 0;

    if (x < 0)
    {
        ind  = -x;
        len +=  x;
        x    =  0;
    }

    if (y < 0 || y >= dims->h - 1 || x >= dims->w - 1)
        return;

    if (len > dims->w - x - 1)
        len = dims->w - x - 1;

    maxind = ind + len;

    term_goto(dims->x + x + 1, dims->y + y + 1);
    for (; ind < maxind; ++ind)
    {
        chr_s *c;
        c = vec_get(chrs, ind);
        chr_print(c, stdout, true);
    }

    fflush(stdout);
}

void frame_clear(frame_s *frm, display_ctx_s *d)
{
    int y;
    frame_dims_s *dims;
    dims = &(frm->dims);

    for (y = 0; y < dims->h - 1; ++y)
        frame_clear_at(frm, d, 0, y);
}

void frame_clear_at(frame_s *frm, display_ctx_s *d, int x, int y)
{
    frame_dims_s *dims;
    dims = &(frm->dims);

    if (y < 0 || y >= dims->h - 1 || x >= dims->w - 1)
        return;

    term_goto(dims->x + x + 1, dims->y + y + 1);

    if (dims->x + dims->w == d->cols + 1)
    {
        printf(TERM_CLR_LINE);
    }
    else
    {
        for (; x < dims->w - 1; ++x)
            printf(" ");
    }

    fflush(stdout);
}

int frame_send_info(frame_s *frm, int id)
{
    packet_s pkt = {
        .type = PACKET_WIN_INFO,
        .data.wininfo = {
            .win  = id,
            .cols = frm->dims.w,
            .rows = frm->dims.h
        }
    };

    if (frm->win)
        packet_send(frm->win, &pkt, NULL);

    return 0;
}

void frame_reset_cursor(frame_s *frm)
{
    frame_dims_s *dims;

    if (!frm) return;

    dims = &(frm->dims);

    term_goto(frm->curx + dims->x, frm->cury + dims->y);
    printf(TERM_SHOW_CUR);
    fflush(stdout);
}
