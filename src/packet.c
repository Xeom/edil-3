
#if !defined(NO_LINUX)
# define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/select.h>

#include "util/sys.h"
#include "util/log.h"
#include "packet.h"

static bool packet_interface_handle_recv(
    packet_interface_s *iface, void *user, packet_s *pkt, vec_s *data)
{
    bool rtn;

    if (pkt->type == PACKET_KILL)
        rtn = false;
    else if (iface->fptr_recv)
        rtn = iface->fptr_recv(iface, user, pkt, data);
    else
        rtn = true;

    vec_kill(data);

    return rtn;
}

static void packet_interface_cleanup(packet_interface_s *iface)
{
    close(iface->sendfd);
    close(iface->recvfd);

    iface->sendfd = -1;
    iface->recvfd = -1;

    pthread_mutex_lock(&(iface->mtx));
    pthread_mutex_unlock(&(iface->mtx));

    pthread_mutex_destroy(&(iface->mtx));

    LOG(INFO, "Packet interface %p cleaned up", iface);
}

static void *packet_interface_thread(void *arg)
{
    packet_interface_s *iface;
    void *user;
    bool alive;
    iface = arg;

    alive = iface->fptr_init(iface, &user);

    while (alive)
    {
        packet_s pkt;
        vec_s    data;

        if (iface->tout.tv_sec || iface->tout.tv_usec)
        {
            if (packet_waiting(iface, iface->tout) &&
                packet_recv(iface, &pkt, &data) == 0)
                alive = packet_interface_handle_recv(iface, user, &pkt, &data);
            else if (iface->fptr_tout)
                alive = iface->fptr_tout(iface, user);
        }
        else
        {
            if (packet_recv(iface, &pkt, &data) == 0)
                alive = packet_interface_handle_recv(iface, user, &pkt, &data);
            else
            {
                LOG(WARN, "Error recieving packets, iface %p, %s",
                    (void *)iface, strerror(errno));
                usleep(100000);
            }
        }
    }

    if (iface->fptr_quit)
        iface->fptr_quit(iface, user);

    if (iface->tofree)
    {
        packet_interface_cleanup(iface);
        pthread_detach(pthread_self());
        LOG(INFO, "Packet iface %p free'd", (void *)iface);
        free(iface);
    }

    LOG(INFO, "Packet iface %p thread ended", (void *)iface);

    return NULL;
}

void packet_interface_init(packet_interface_s *iface)
{
    int fds[2];

    pipe(fds);

    iface->recvfd = fds[0];
    iface->sendfd = fds[1];

    pthread_mutex_init(&(iface->mtx), NULL);
    pthread_create(&(iface->thread), NULL, packet_interface_thread, iface);

    LOG(INFO, "Packet interface %p created", iface);
}

void packet_interface_kill(packet_interface_s *iface)
{
    struct packet_s pkt = { .type = PACKET_KILL };

    packet_send(iface, &pkt, NULL);

#if defined(_GNU_SOURCE)
    struct timespec tout = { .tv_sec = time(NULL) + 10, .tv_nsec = 0 };

    if (pthread_timedjoin_np(iface->thread, NULL, &tout) != 0)
    {
        LOG(ERR, "Packet interface %p failed to terminate and timed out",
            (void *)iface);
        pthread_cancel(iface->thread);
    }
#else
    pthread_join(iface->thread, NULL);
#endif

    LOG(INFO, "Packet interface %p joined", iface);

    packet_interface_cleanup(iface);
}

void packet_send(packet_interface_s *iface, packet_s *packet, vec_s *data)
{
    pthread_mutex_lock(&(iface->mtx));

    packet->tid = sys_tid();
    if (data)
    {
        packet->bytes = data->usage;
        packet->width = data->width;
    }

    if (iface->sendfd == -1) return;

    write(iface->sendfd, packet, sizeof(packet_s));

    if (data)
    {
        write(iface->sendfd, vec_first(data), data->usage);
    }

    pthread_mutex_unlock(&(iface->mtx));

    LOG(PKTS, "Packet sent from thread %d to iface %p, type %d, %lu bytes",
        packet->tid, (void *)iface, packet->type, packet->bytes);

}

int packet_recv(packet_interface_s *iface, packet_s *packet, vec_s *data)
{
    long rtn;

    if (iface->recvfd == -1) return -1;

    rtn = read(iface->recvfd, packet, sizeof(packet_s));

    if (rtn < (long)sizeof(packet_s))
        return -1;

    LOG(PKTS, "Packet received from thread %d at iface %p, type %d, %lu bytes",
        packet->tid, (void *)iface, packet->type, packet->bytes);

    if (packet->bytes)
    {
        size_t bytes;

        vec_init(data, packet->width);
        vec_resize(data, packet->bytes / packet->width);

        bytes = 0;

        while (bytes < packet->bytes)
            bytes += read(iface->recvfd, &(data->data[bytes]), packet->bytes - bytes);
    }
    else
        vec_init(data, sizeof(int));

    return 0;
}

bool packet_waiting(packet_interface_s *iface, struct timeval tout)
{
    fd_set fds;

    FD_ZERO(&fds);
    FD_SET(iface->recvfd, &fds);

    select(iface->recvfd + 1, &fds, NULL, NULL, &tout);

    return FD_ISSET(iface->recvfd, &fds);
}

packet_interface_s *packet_interface_create(
    void (*startf)(packet_interface_s *))
{
    packet_interface_s *rtn;

    rtn = malloc(sizeof(packet_interface_s));
    startf(rtn);
    rtn->tofree = true;

    return rtn;
}
