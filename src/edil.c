#include <signal.h>
#include <unistd.h>

#include "util/sys.h"
#include "core.h"

int main(void)
{
    sys_sig_block(SIGWINCH);
    core_init();

    return 0;
}
