#include <stdlib.h>
#include <string.h>

#include "container/vec.h"

static int vec_shrink(vec_s *v);
static int vec_grow  (vec_s *v);

static int vec_shrink(vec_s *v)
{
    if (v->usage >= (v->capacity >> 2)) return 0;

    if (!(v->resizable)) return 0;

    do
    {
        v->capacity >>= 1;
    } while (v->usage < (v->capacity >> 2));

    v->data = realloc(v->data, v->capacity);

    if (v->data)
        return 0;
    else
        return -1;
}

static int vec_grow(vec_s *v)
{
    if (v->usage <= v->capacity) return 0;

    if (!(v->resizable)) return -1;

    if (v->capacity == 0)
        v->capacity = 1;

    do
    {
        v->capacity <<= 1;
    } while (v->usage > v->capacity);

    v->data = realloc(v->data, v->capacity);

    if (v->data)
        return 0;
    else
        return -1;

}

void vec_init(vec_s *v, size_t width)
{
    v->width     = width;
    v->usage     = 0;
    v->capacity  = 0;
    v->data      = NULL;
    v->resizable = true;
}

void vec_init_from_mem(vec_s *v, size_t width, void *mem, size_t num)
{
    v->width     = width;
    v->usage     = 0;
    v->capacity  = num * width;
    v->data      = (char *)mem;
    v->resizable = false;
}

void vec_kill(vec_s *v)
{
    if (v->data)
        free(v->data);

    v->data = NULL;
    v->capacity = 0;
}

int vec_resize(vec_s *v, size_t num)
{
    size_t oldusage;
    oldusage = v->usage;
    v->usage = num * v->width;

    if (v->usage > oldusage)
        return vec_grow(v);
    else if (v->usage < oldusage)
        return vec_shrink(v);

    return 0;
}

int vec_sort(vec_s *v, int (*f)(const void *a, const void *b))
{
    if (!v || !f)      return -1;
    if (v->usage == 0) return 0;

    qsort(v->data, vec_len(v), v->width, f);

    return 0;
}

void *vec_rep(vec_s *v, size_t ind, size_t n, const void *data, size_t reps)
{
    /* The bytes after the point of insertion, the offset into the data   *
     * of the point of insertion, and the number of bytes being inserted, *
     * and the number of bytes per single insertion.                      */
    size_t bytesafter, offset, bytesins, bytessing;

    if (!v) return NULL;
    if (n == 0 || reps == 0) return NULL;

    offset     = ind * v->width;
    bytessing  = n   * v->width;
    bytesins   = bytessing * reps;
    bytesafter = v->usage - offset;

    /* Don't accept inserts beyond the end of the data */
    if (offset > v->usage) return NULL;

    /* Increment and resize before putting in the data */
    v->usage  += bytesins;
    if (vec_grow(v) == -1) return NULL;

    /* Shift the bytes after the point of insertion forward */
    if (bytesafter)
        memmove(v->data + offset + bytesins, v->data + offset, bytesafter);

    /* Fill the gap, either with data or to zero */
    if (data)
    {
        while (reps--)
        {
            memmove(v->data + offset, data, bytessing);
            offset += bytessing;
        }
    }
    else memset(v->data + offset, 0, bytesins);

    return v->data + offset;
}

int vec_del(vec_s *v, size_t ind, size_t n)
{
    size_t bytesafter, bytesdead, offset;

    if (!v)     return -1;
    if (n == 0) return 0;

    offset     = ind * v->width;
    bytesdead  = n   * v->width;
    bytesafter = v->usage - offset - bytesdead;

    if (offset + bytesdead > v->usage) return -1;

    if (bytesafter)
        memmove(&v->data[offset], &v->data[offset + bytesdead], bytesafter);

    v->usage -= bytesdead;

    return vec_shrink(v);
}

void *vec_get(vec_s *v, size_t ind)
{
    size_t offset;

    if (!v) return NULL;

    offset = ind * v->width;

    if (offset >= v->usage) return NULL;

    return &v->data[offset];
}
