#include <unistd.h>
#include <signal.h>

#include "packet.h"
#include "module/editor/core.h"
#include "module/splitter/core.h"
#include "module/welcome/core.h"
#include "module/display/core.h"
#include "module/input/core.h"
#include "module/buffers/core.h"
#include "util/sys.h"


#include "core.h"

packet_interface_s line_splitter;
packet_interface_s display;
packet_interface_s input;
packet_interface_s buffers;

void core_init(void)
{
    packet_interface_s *viewer;
    vec_s text;
    vec_init(&text, sizeof(chr_s));
    chr_from_str(&text, "Hello!");

    sys_sig_block(SIGWINCH);

    input_start(&input);
    splitter_start(&line_splitter);
    buffers_start(&buffers);

    display_start(&display);
    display_send_win_iface(&display, 0, packet_interface_create(welcome_start));

    viewer = packet_interface_create(editor_start);
    display_send_split(&display, 0, WIN_SPLIT_VERTICAL, viewer);

    buffers_send_open(viewer);
    buffers_send_ins_line(0, 0, 5);
    buffers_send_set_line(0, 0, &text);

    usleep(1000000);
    buffers_send_set_line(0, 1, &text);
    usleep(1000000);
    buffers_send_set_line(0, 2, &text);
    usleep(1000000);
    display_send_close(&display, 0);

    usleep(500000);
    packet_interface_kill(&display);
    packet_interface_kill(&line_splitter);
    packet_interface_kill(&input);
}
