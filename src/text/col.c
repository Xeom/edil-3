#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "text/col.h"

static col_desc_s col_descs[] =
{
    [COL_DEFAULT]        = { .attrs = COL_NONE },
    [COL_WIN_FRAME]      = { .fg = { 0x3a, 0x5f, 0xcd }, .attrs = COL_NOBG },
    [COL_WIN_FRAME_SEL]  = { .fg = { 0xff, 0x55, 0x55 }, .attrs = COL_NOBG }
};

static void col_rgb_print(col_rgb_s *rgb, bool bg)
{
    printf(";%d;2;%d;%d;%d",
        bg ? 48 : 38,
        (int)rgb->r, (int)rgb->g, (int)rgb->b);
}

static void col_desc_print(col_desc_s *desc)
{
    static col_desc_s prev;

    if (memcmp(desc, &prev, sizeof(col_desc_s)) == 0)
        return;

    printf("\033[0");

    if ((desc->attrs & COL_NOFG) == 0)
        col_rgb_print(&(desc->fg), false);

    if ((desc->attrs & COL_NOBG) == 0)
        col_rgb_print(&(desc->bg), true);

    if (desc->attrs & COL_UNDER) printf(";4");
    if (desc->attrs & COL_REV)   printf(";7");
    if (desc->attrs & COL_FLASH) printf(";5");

    printf("m");
}

void col_print(col_t col)
{
    col_desc_print(&col_descs[col]);
}
