#include <string.h>
#include <stdarg.h>

#include "text/chr.h"

chr_utf8_t chr_get_utf8_type(unsigned char c)
{
    if (c == 0x00) return CHR_UTF8_INV;
    if (c <= 0x7f) return CHR_UTF8_ASCII;
    if (c <= 0xbf) return CHR_UTF8_PART; /* This is an error :( */
    if (c <= 0xdf) return CHR_UTF8_2WIDE;
    if (c <= 0xef) return CHR_UTF8_3WIDE;
    if (c <= 0xf7) return CHR_UTF8_4WIDE;
    if (c <= 0xfb) return CHR_UTF8_5WIDE;
    if (c <= 0xfd) return CHR_UTF8_6WIDE;
    else           return CHR_UTF8_INV;
}

int chr_utf8_type_len(chr_utf8_t type)
{
    if (type == CHR_UTF8_PART) return -1;
    if (type == CHR_UTF8_INV)  return -1;

    return (int)type;
}

int chr_utf8_len(chr_s *chr)
{
    chr_utf8_t type;

    type = chr_get_utf8_type((unsigned char)(chr->utf8[0]));

    return chr_utf8_type_len(type);
}

int chr_width(chr_s *chr)
{
    if (chr->blank) return 0;

    return strlen(chr->utf8);
}

void chr_print(chr_s *chr, FILE *stream, bool colour)
{
    int len;

    if (chr->blank) return;

    len = chr_utf8_len(chr);
    if (len == -1) return;

    fwrite(chr->utf8, 1, len, stream);
}

uint32_t chr_codepoint(chr_s *chr)
{
    int len, ind;
    uint32_t rtn;

    if (chr->blank) return 0;

    len = chr_utf8_len(chr);
    if (len == -1) return 0;
    if (len ==  1) return chr->utf8[0];

    rtn = (0x7f >> len) & chr->utf8[0];

    for (ind = 1; ind < len; ++ind)
    {
        rtn <<= 6;
        rtn  |= (chr->utf8[ind]) & 0x3f;
    }

    return rtn;
}

int chr_is_whitespace(chr_s *chr)
{
    return (chr->blank               ||
        strcmp(chr->utf8, " ")  == 0 ||
        strcmp(chr->utf8, "\t") == 0);
}

int chr_decode(chr_s *chr, char c)
{
    chr_utf8_t type;

    type = chr_get_utf8_type(c);

    if (chr->utf8[0] == '\0')
    {
        chr->utf8[0] = c;

        if (type >= CHR_UTF8_ASCII &&
            type <= CHR_UTF8_6WIDE)
            return 1;

        switch (type)
        {
        case CHR_UTF8_ASCII:
            return 1;

        case CHR_UTF8_2WIDE:
        case CHR_UTF8_3WIDE:
        case CHR_UTF8_4WIDE:
        case CHR_UTF8_5WIDE:
        case CHR_UTF8_6WIDE:
            return 0;

        default:
            memset(chr, 0, sizeof(chr_s));
            return 0;
        }
    }
    else
    {
        int len, slen;
        len  = chr_utf8_len(chr);
        slen = strlen(chr->utf8);

        if (len == -1   ||
            slen >= len ||
            slen >= 6   ||
            type != CHR_UTF8_PART)
        {
            memset(chr, 0, sizeof(chr_s));
            return 0;
        }

        chr->utf8[slen] = c;

        if (slen + 1 == len)
            return 1;

        else
            return 0;
    }
}

void chr_from_mem(vec_s *chrs, char *str, size_t len)
{
    chr_s  chr;
    size_t ind;

    memset(&chr, 0, sizeof(chr_s));

    for (ind = 0; ind < len; ++ind)
    {
        if (chr_decode(&chr, str[ind]) == 1)
        {
            vec_app(chrs, &chr);
            memset(&chr, 0, sizeof(chr_s));
        }
    }
}

void chr_from_str(vec_s *chrs, char *str)
{
    size_t len;
    len = strlen(str);

    chr_from_mem(chrs, str, len);
}

void chr_from_vec(vec_s *chrs, vec_s *str)
{
    size_t len;
    len = vec_len(str);

    chr_from_mem(chrs, vec_first(str), len);
}

void chr_format(vec_s *chrs, char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    chr_va_format(chrs, fmt, args);
    va_end(args);
}

void chr_va_format(vec_s *chrs, char *fmt, va_list args)
{
    vec_s str;
    int len = 64;
    int written;

    vec_init(&str, sizeof(char));
    vec_resize(&str, len);

    written = vsnprintf(vec_first(&str), len, fmt, args);

    if (written < 0)
    {
        return;
    }

    vec_resize(&str, written);

    if (written > len)
    {
        written = vsnprintf(vec_first(&str), written, fmt, args);
    }

    chr_from_vec(chrs, &str);
    vec_kill(&str);
}

void chr_to_str(vec_s *chrs, vec_s *str)
{
    VEC_FOREACH(chrs, chr_s *, chr)
    {
        int width;

        if (chr->blank) continue;

        width = chr_width(chr);
        if (width <= 0) continue;

        vec_add(str, width, chr->utf8);
    }
}
