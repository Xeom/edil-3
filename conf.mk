FILES=container/vec          \
      container/table        \
      packet                 \
      core                   \
      module/splitter/core   \
      module/display/core    \
      module/display/display \
      module/display/frame   \
      module/buffers/line    \
      module/buffers/buffer  \
      module/buffers/core    \
      module/welcome/core    \
      module/input/core      \
      module/input/parser    \
      module/input/keys      \
      module/editor/core     \
      module/editor/editor   \
      text/chr               \
      text/col               \
      util/term              \
      util/file              \
      util/log               \
      util/sys

SRCDIR=src/
INCDIR=inc/
OBJDIR=obj/
BINDIR=bin/
DEPFILE=deps.d

INSTDIR=/usr/local/bin/

VERSION=0.2.0

WARNINGS=all extra no-unused-parameter no-switch missing-prototypes

STD=gnu11

DEFINES=
